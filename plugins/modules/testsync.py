#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = r'''
---
module: testsync
short_description: testsync
options:
  src:
    description:
      - Path on the source host
   type: str
    required: true
  use_ssh_args:
    description:
      - Use the ssh_args specified in ansible.cfg.
    type: bool
    default: no
'''

EXAMPLES = '''
'''


from ansible.module_utils.basic import AnsibleModule


def main():
    module = AnsibleModule(
        argument_spec=dict(
            src=dict(type='str', required=True),
            ssh_args=dict(type='str'),
        ),
        supports_check_mode=True,
    )
    src = module.params['src']
    ssh_args = module.params['ssh_args']
    msg = f"And testsync.py src={src} ssh_args={ssh_args}"
    return module.exit_json(changed=False, msg=msg,
                            rc=0, stdout_lines=msg)


if __name__ == '__main__':
    main()
