```
git clone https://gitlab.com/justinc/test-sshargs.git
cd test-sshargs

mkdir -p /tmp/ansible_collections/tester
ln -s $PWD /tmp/ansible_collections/tester/sshargs
ls -al /tmp/ansible_collections/tester/sshargs

python3.10 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt
ansible-galaxy collection install ansible.posix:1.4.0

ansible-playbook -i localhost, play1.yml -v
```
